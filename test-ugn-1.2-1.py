import random
import time
from datetime import date

import numpy as np
import ugn_1_2_pytorch as ugn


# noinspection PyBroadException
def check_correctness(str_gen):
    chunks = str_gen.split('-')
    try:
        d = date(int(chunks[0]), int(chunks[1]), int(chunks[2]))
        return True
    except:
        return False


def get_correct(generations):
    return [g for g in generations if check_correctness(g)]


def compute_correctness_ratio(generations):
    return len(get_correct(generations)) / len(generations)


def compute_error_ratio(generations):
    return 1 - compute_correctness_ratio(generations)


def compute_innov_ratio(train_data, generations):
    return (len(generations) - len(set(train_data).intersection(generations))) / len(generations)


def compute_actual_innov_ration(train_data, generations):
    return compute_innov_ratio(train_data, generations) * compute_correctness_ratio(generations)


def compute_innov_2(generations):
    return len(set(generations)) / len(generations)


def print_infos(train_data, generations):
    nf = '{:.4f}'
    correct_generations = get_correct(generations)
    error_ratio = nf.format(compute_error_ratio(generations))
    innov_ratio = nf.format(compute_actual_innov_ration(train_data, correct_generations))
    nd_innov_ratio = nf.format(compute_innov_2(correct_generations))

    print('--infos--')
    print('---------')
    print('error ratio:\t\t', error_ratio)
    print('innov ratio:\t\t', innov_ratio)
    print('nd innov. ratio:\t', nd_innov_ratio)


train_data = [date(random.randint(1970, 2015),
                   random.randint(1, 12),
                   random.randint(1, 28)).strftime('%Y-%m-%d')
              for _ in
              range(8000)]

counter_example_data = [s[:5] + '00' + s[7:] for s in train_data[:200]]
counter_example_data += [s[:8] + '00' + s[10:] for s in train_data[200:400]]

network = ugn.Network(11, max_degree=13)

print('fit...')
start = time.time()

for td in train_data:
    network.fit(td)

for ctd in counter_example_data:
    network.fit(ctd, -1)

end = time.time()

print('time elapsed : ', "{:.4f}".format(end - start), 's')

network.fix()


print('fixed')

s1 = network.similarity(np.array(list("11-03-1986")))
s2 = network.similarity(np.array(list("456841123")))

s3 = network.similarity(np.array(list("1932-09-12")))
s4 = network.similarity(np.array(list("1988-05-23")))
s6 = network.similarity(np.array(list("1981-01-01")))
s5 = network.similarity(np.array(list("----------")))
s7 = network.similarity(np.array(list("a-1-8798-b")))


print('generate...')

start = time.time()

nb = 250

raw_generations = [network.generate() for _ in range(nb)]

end = time.time()

ok = [g for g in raw_generations]
generations = [''.join(g) for g in ok]

print(generations)
print('time elapsed : ', "{:.4f}".format(end - start), 's')

print_infos(train_data, generations)
