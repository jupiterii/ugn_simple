"""
Stable example of date generation and recognize with pytorch
Release notes :
2020-04-27
 - Use of cuda with pytorch
 - Improve fix neural network with round
 - Add counter-example for better effectiveness (parameter weight in fit function)
 - Add fit_many for fitting a collection of samples
 - Add recognize function (similarity response of network)
 - Refactoring
"""

import functools as fct
import itertools

import numpy as np
import torch
from sklearn.preprocessing import Normalizer

import pytorch_utils as ptu
from vec_encoder import UDEncoder

# Init cuda
print('Cuda is available : ' + str(torch.cuda.is_available()))
print('Cuda device number : ' + str(torch.cuda.device_count()))
cuda = torch.device('cuda')


class Network:

    def __init__(self, size, max_degree):
        self.__encoder = UDEncoder()

        self.max_degree = max_degree
        self.size = size + 1
        self.matrix_stack = [np.zeros((self.size, self.size)) for _ in range(self.max_degree)]
        self.fixed_matrix_stack = None

        self.base = torch.zeros((1, self.size), device=cuda)

    def get_encoder(self):
        return self.__encoder

    def fit_many(self, samples, weight=1.):
        for sample in samples:
            self.fit(sample, weight)

    def fit(self, sample, weight=1.):
        """ Train network with a vector sample """

        # begin to vector content adjacency
        for i, e in enumerate(sample):
            self.__reinforcement(UDEncoder.ofb, e, i, weight)

        # vector content to vector content adjacency
        for i, e1 in enumerate(sample):
            for j, e2 in enumerate(sample):
                if i < j:
                    k = j - i - 1
                    self.__reinforcement(e1, e2, k, weight)

        # end to vector content adjacency
        for i, e in enumerate(reversed(sample)):
            self.__reinforcement(e, UDEncoder.ofb, i, weight)



    def __reinforcement(self, e1, e2, degree, weight):

        if degree >= self.max_degree:
            return

        # encode elements
        x, y = (self.__encoder.encode(e1), self.__encoder.encode(e2))
        # get matrix for degree
        mat = self.matrix_stack[degree]

        # reinforce weight adjacency between elements
        mat[x, y] += weight

        # coordinate of adjacency
        return x, y


    def fix(self):
        # normalize weight
        self.fixed_matrix_stack = [torch.tensor(ms, device=cuda) for ms in self.matrix_stack]
        self.fixed_matrix_stack = [ptu.norm_max(t) for t in self.fixed_matrix_stack]
        self.fixed_matrix_stack = [((t * 10).round()) / 10 for t in self.fixed_matrix_stack]



    def __propagate(self, sm_mat, adj_mat, degree=0, degree_func=None):
        mul = degree_func(degree) if degree_func else 1.
        # with floor (with eval)
        return ptu.noise(sm_mat.floor().matmul(adj_mat), cuda, 2.) * mul


    def __propagate_shortmemory(self, sm_mats, adj_mats, max_degree):
        # propagation for each degree in function of short memory
        # slice list
        p = itertools.islice(reversed(sm_mats), max_degree)
        prop_mat = (self.__propagate(sm_mat, adj_mats[i]) for i, sm_mat in enumerate(p))
        # cumulative score of propagations
        return fct.reduce(lambda m1, m2:  m1 + m2, prop_mat, self.base)



    def __create_stimuli_mat(self, index):
        mat = np.zeros((1, self.size))
        if index < self.size:
            mat[0, index] = 1.

        return torch.tensor(mat, device=cuda)

    def __create_stimulis(self, vector):
        return [self.__create_stimuli(e) for e in vector or []]


    def __create_stimuli(self, e):
        i = self.__encoder.encode(e)
        return self.__create_stimuli_mat(i)


    # pass normalized mat as parameter !
    def __get_activation_value(self, norm_mat):
        # find coordinate of max value
        max_idx = norm_mat.argmax()
        # decode coordinate, give the value
        return self.__encoder.decode(max_idx.item())

    def generate(self, seed=None):

        sm_mats = [self.__create_stimuli(UDEncoder.ofb)] + self.__create_stimulis(seed)

        while True:

            mat_norma = self.generate_next(sm_mats)

            # get max value
            val = self.__get_activation_value(mat_norma)

            # if value is end, break
            if val == UDEncoder.ofb:
                break

            # append propagation to memory stack (short memory)
            sm_mats.append(mat_norma)

        return [self.__get_activation_value(m) for m in sm_mats][1:]

    def generate_next(self, sm_mats):

        mat_cumul = self.__propagate_shortmemory(sm_mats, self.fixed_matrix_stack, self.max_degree)
        # normalize
        return mat_cumul / mat_cumul.max()

    def similarity(self, vector):

        cos_similarities = []
        ofb_stimuli = self.__create_stimuli(UDEncoder.ofb)
        sm_mats = [ofb_stimuli]

        # does generate many possibilities ?
        predicted_state = [self.generate_next(sm_mats) for _ in range(20)]

        for i in range(len(vector)):
            state = self.__create_stimuli(vector[i])
            # cos_similarity = torch.nn.functional.cosine_similarity(state, predicted_state).item()
            cos_similarity = max([torch.nn.functional.cosine_similarity(state, x).item() for x in predicted_state])

            cos_similarities.append(cos_similarity)
            sm_mats.append(state)
            # predicted_state = self.generate_next(sm_mats)
            predicted_state = [self.generate_next(sm_mats) for _ in range(20)]

        # Check out of bound for end
        cos_similarity = max([torch.nn.functional.cosine_similarity(ofb_stimuli, x).item() for x in predicted_state])
        cos_similarities.append(cos_similarity)

        total = sum(cos_similarities) / len(cos_similarities)

        return total
