class CDEncoder:
    """
    Contiguous Discrete Encoder
    """
    # special char
    ofb = 0x00

    def encode(self, value):
        return value

    def decode(self, i):
        return i

class UDEncoder:
    """
    Uncontiguous Discrete Encoder
    """
    # special char
    ofb = 0x00
    #ofb = '\0'

    def __init__(self):
        self.__i = 0
        self.__encoding_dict = dict()
        self.__decoding_dict = dict()
        self.encode(UDEncoder.ofb)

    def encode(self, value):
        # value already exist
        if value in self.__encoding_dict:
            return self.__encoding_dict[value]

        # fill encoding and decoding dictionary for given value
        current = self.__i
        self.__encoding_dict[value] = current
        self.__decoding_dict[current] = value

        # increase index for next element
        self.__i += 1

        # return index of encoded element
        return current

    def decode(self, i):
        return self.__decoding_dict.get(i, None)

    def get_dict_size(self):
        return self.__i - 1

    def __str__(self):
        return str(self.__encoding_dict)


class UDEncoder2:
    """
    Uncontiguous Discrete Encoder
    """
    # special char
    # ofb = 0x00
    ofb = '\0'

    def __init__(self):
        self.__i = 0
        self.__encoding_dict = dict()
        self.__decoding_dict = dict()

    def encode(self, value):
        # value already exist
        if value in self.__encoding_dict:
            return self.__encoding_dict[value]

        # fill encoding and decoding dictionary for given value
        current = self.__i
        self.__encoding_dict[value] = current
        self.__decoding_dict[current] = value

        # increase index for next element
        self.__i += 1

        # return index of encoded element
        return current

    def decode(self, i):
        return self.__decoding_dict.get(i, None)

    def get_dict_size(self):
        return self.__i - 1

    def __str__(self):
        return str(self.__encoding_dict)


class UDGramEncoder:
    """
    Uncontiguous Discrete n-gram Encoder
    """
    # special char
    # ofb = 0x00
    ofb = '\0'

    def __init__(self):
        self.__i = 0
        self.__encoding_dicts = dict()
        self.__decoding_dicts = dict()

    def __get_or_create_key(self, key):
        """ Get or create new key associated with encoding and decoding dictionaries """
        if key not in self.__encoding_dicts:
            self.__encoding_dicts[key] = dict()
            self.__decoding_dicts[key] = dict()

        return self.__encoding_dicts[key], self.__decoding_dicts[key]

    def encode(self, ngram_level, value):
        encoding_dict, decoding_dict = self.__get_or_create_key(ngram_level)
        # value already exist
        if value in encoding_dict:
            return encoding_dict[value]

        # fill encoding and decoding dictionary for given value
        #current = self.__i
        current = len(encoding_dict)
        encoding_dict[value] = current
        decoding_dict[current] = value

        # increase index for next element
        #self.__i += 1

        # return index of encoded element
        return current

    def decode(self, ngram_level, i):
        _, decoding_dict = self.__get_or_create_key(ngram_level)
        return decoding_dict.get(i, None)

    def get_dict_size(self):
        return self.__i - 1

    def __str__(self):
        return str(self.__encoding_dicts)
