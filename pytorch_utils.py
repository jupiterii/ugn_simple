import torch
import numpy as np

def norm_max(tensor, dim=1):
    """ Normalize tensor on requested dimension using max value """
    max_indices = tensor.max(dim=dim)
    max_values = max_indices.values.unsqueeze(1) + 0.000001
    return torch.div(tensor, max_values)

def norm_max_vec(vector):
    max_val = vector.max().item()
    return vector / max_val


def step(norm_tensor):
    return ((norm_tensor * 10).round()) / 10

def noise(matrix, device, multiplier=0.2):
    """ Element wise noise a matrix (2d tensor) (uniform distribution) """
    matrix_heaviside = matrix.sign()
    mat_noise = torch.tensor(np.random.uniform(size=matrix_heaviside.shape), device=device)
    mat_noise_heavi = mat_noise * matrix_heaviside
    mat_noised = matrix + (mat_noise_heavi * multiplier)
    return mat_noised

def noise2(matrix, device, multiplier=1.):
    """ Element wise noise a matrix (2d tensor) (uniform distribution) """
    mat_noise = torch.tensor(np.random.uniform(size=matrix.shape), device=device)
    mat_noised = mat_noise * matrix
    return mat_noised

def propagate(memory_vec, adj_t, device):
    return noise(memory_vec.matmul(adj_t), device=device)

def propagate2(memory_vec, adj_t, device):
    return memory_vec.matmul(noise2(adj_t, device=device))
    # return noise2(memory_vec.matmul(adj_t), device=device)

def matrix_similarity(m1, m2):
    return (torch.nn.functional.cosine_similarity(m1, m2).sum()).item()